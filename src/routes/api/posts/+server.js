/**
 * @typedef {('asc' | 'desc')} order
 */

import { json } from '@sveltejs/kit';
import db from '$lib/database';

/** @type {import('./$types').RequestHandler} */
export async function GET({ url }) {
	const limitParam = url.searchParams.get('limit');
	const limit = limitParam ? parseInt(limitParam) : 30;

	const orderParam = url.searchParams.get('order');
	const order = /** @type {order} */ (orderParam ? orderParam : 'asc');

	const posts = await db.post.findMany({
		orderBy: { id: order },
		take: limit
	});

	return json(posts);
}
