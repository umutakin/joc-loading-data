/**
 * @param {*} event
 * @returns @type { import("@sveltejs/kit").RequestHandler}
 */
export const GET = async (event) => {
	/** @type { ResponseInit } */
	const options = {
		status: 418,
		headers: {
			X: 'Gon give it to ya'
		}
	};
	return new Response('Hello', options);
};

/**
 * @param {import("@sveltejs/kit").RequestEvent} event
 * @returns @type { import("@sveltejs/kit").RequestHandler}
 */
export const POST = async (event) => {
	const data = await event.request.formData();
	const email = data.get('email');

	// subcribe to newsletter
	console.log(email);

	return new Response(JSON.stringify({ success: true }), {
		headers: {
			'Content-Type': 'application/json'
		}
	});
};
