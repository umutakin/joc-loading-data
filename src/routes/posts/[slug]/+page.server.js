import { error } from '@sveltejs/kit';
import db from '$lib/database';

/** @type {import('./$types').PageServerLoad} */
export async function load({ params, parent }) {
	const parentData = await parent();
	console.log(parentData);

	const post = await db.post.findUnique({
		where: { slug: params.slug }
	});

	if (!post) {
		throw error(404, 'Post not found');
	}

	return { post };
}
