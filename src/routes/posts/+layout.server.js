import db from '$lib/database';

/** @type {import('./$types').LayoutServerLoad} */
export async function load(event) {
	const posts = db.post.findMany({
		select: {
			title: true,
			slug: true
		},
		take: 4
	});

	return { posts };
}
