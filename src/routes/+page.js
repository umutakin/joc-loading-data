/** @type {import('./$types').PageLoad} */
export async function load({ fetch, depends }) {
	const random = Math.round(Math.random() * 30);

	const response = await fetch(`/api/posts?limit=${random}`);
	/** @type {import('@prisma/client').Post[]} */
	const posts = await response.json();

	depends('posts');

	return { posts };
}
